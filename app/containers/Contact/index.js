import React from 'react';
import {View} from 'react-native';

import TextField from '../../components/TextField';
import ButtonGroup from '../../components/ButtonGroup';
import {styles} from './styles';

const Contact = ({route, onDelete, onCall}) => {
  const {name, number} = route.params;

  return (
    <View style={styles.container}>
      <TextField label="Name" value={name} />
      <TextField label="Phone number" value={number} />
      <ButtonGroup onDelete={onDelete} onCall={onCall} />
    </View>
  );
};

export default Contact;
