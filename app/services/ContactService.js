import {PermissionsAndroid} from 'react-native';
import Contacts from 'react-native-contacts';
import call from 'react-native-phone-call';

export const requestForPermissions = () => {
  return PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
    {
      title: 'Contacts',
      message: 'This app would like to view your contacts.',
      buttonPositive: 'Please accept bare mortal',
    },
  );
};

export const getContacts = (callback, filter = () => true) => {
  return Contacts.getAll((err, contacts) => {
    if (err === 'denied') {
      throw new Error('Permissions denied');
    } else {
      callback(contacts.filter(filter));
    }
  });
};

export const openContactForm = (callback) => {
  Contacts.openContactForm({}, callback);
};

export const deleteContact = (contact, callback) => {
  Contacts.deleteContact(contact, callback);
};

export const callTo = (number) => {
  call({number: number.replace(/\D/g, '')});
  console.log(number);
};
