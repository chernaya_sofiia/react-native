import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

import {styles} from './styles';

const CustomButton = ({text, onPress, color}) => (
  <TouchableOpacity style={[styles.btn, styles[color]]} onPress={onPress}>
    <Text style={[styles.text, styles[color]]}>{text}</Text>
  </TouchableOpacity>
);

export default CustomButton;
