import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  btn: {
    paddingLeft: 24,
    paddingRight: 24,
    paddingTop: 6,
    paddingBottom: 6,
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 5,
  },
  text: {
    fontSize: 18,
  },
  primary: {
    borderColor: '#007bff',
    color: '#007bff',
  },
  danger: {
    borderColor: 'transparent',
    color: '#dc3545',
  },
});
