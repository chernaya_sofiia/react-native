import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import {styles} from './styles';

const Contact = ({name, onPress}) => (
  <TouchableOpacity onPress={onPress}>
    <View style={styles.container}>
      <Text style={styles.text}>{name}</Text>
    </View>
  </TouchableOpacity>
);

export default Contact;
