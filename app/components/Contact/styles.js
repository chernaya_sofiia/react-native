import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    borderStyle: 'solid',
    paddingTop: 18,
    paddingBottom: 18,
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
