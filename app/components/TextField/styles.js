import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    marginTop: 4,
    marginBottom: 24,
  },
  label: {
    fontWeight: 'bold',
    marginBottom: 12,
  },
  text: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    borderStyle: 'solid',
    fontSize: 18,
    paddingBottom: 6,
  },
});
