import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  root: {
    position: 'absolute',
    bottom: 24,
    right: 24,
    borderRadius: 100,
    backgroundColor: '#4bb25d',
  },
  text: {
    color: '#fff',
    width: 80,
    height: 80,
    fontSize: 36,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});
